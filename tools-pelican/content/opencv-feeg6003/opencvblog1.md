title: Introduction to OpenCV
authors: Jose Miguel Garro, Shaokang Zhang
date: 2018-03-13
tags: vision, python, opencv, training
slug: post

# Getting started with the VM #
___

Find out the **opencv.ova** VM for this Workshop at [ngcmbits](http://www.southampton.ac.uk/~ngcmbits/virtualmachines/) (available soon). If you are using a new VM follow the next steps. On the other hand, if you have downloaded the provided machine jump to the **Workshop material** section to pull the latest changes in the repository.
~~~
$ sudo apt-get install update
~~~

After updating the sources of the repositories, it is time to install some dependencies, normally these ones should be available in a full Ubuntu version but as we are dealing with a minimal version it is required to do it manually.

~~~
$ sudo apt-get install gcc make
~~~

The next step is to install the Guest additions. As you probably have noticed the screen of your VM does not fit your real resolution, this is one of the main features that is going to solve the installation of the additions. 

Go to the Virtual Box options, find the Devices tab and click Insert Guest Additions. After this step, open a terminal window and type the following command:

~~~
$ cd /media/feeg6003/VirtualBox...
$ sudo sh ./VBoxLinuxAdditions.run
~~~

Once the installation is completed you will be asked to restart your VM. Please do so and you will be able to fit the screen to the real resolution using the desktop preferences.

# OpenCV - Python Installation #
___

![OpenPython]({filename}/opencv-feeg6003/images/opencvpython.png)

To perform the installation of the OpenCv library for python there are two ways to do it:

* The first one is installing only the libraries for python using pre-built libraries typing the following commands:

~~~
$ sudo apt-get install python-opencv
$ sudo apt-get install python-numpy
$ sudo apt-get install python-matplotlib
$ sudo apt-get install ffmpeg
~~~

To check if everything it is been successfully installed open a Python IDLE and type the following commands:

~~~python
import cv2
print cv2.__version__
~~~

If the version displayed is 3.1.0 or greater you are in good position to continue.

* The second option, is to download the source code and compile the whole code. This is a long and tedious installation, so the recommendation is to do it with the previous commands.


Check “Building OpenCV from source” to follow the steps. Consult the [OpenCV guide](https://docs.opencv.org/trunk/d2/de6/tutorial_py_setup_in_ubuntu.html)

# Workshop material #
___

## Pulling last version of the repository examples ##

In order to get the samples to complete and the solutions from the workshop, the next command can be used to download all the sources from Bitbucket. Inside the repository you will be able to finde a folder named 'examples' where to can find all of this codes also and the images and videos used over this blog.

~~~
$ cd $HOME
$ hg clone https://josemgf@bitbucket.org/josemgf/opencv-feeg6003
$ cd opencv-feeg6003
$ hg pull
$ hg update
~~~

# OpenCV #
___

![OpenCV]({filename}/opencv-feeg6003/images/opencv.png)

OpenCV is an open source library used for image processing and machine learning. It was built to provide a common infrastructure for computer vision applications. 

The library contains more than 2500 algorithms, from the basics of getting images and changing the color to the most complex algorithms to extract 3d objects from an image. Passing through removing the red eyes and tracking objects for example.

This is a cross-platform library having available interfaces for C, C++, MATLAB, Python and Java alongside Linux, Windows, Android and MacOS.

![os]({filename}/opencv-feeg6003/images/oa.jpg )

## Getting started with OpenCV ##

To get started with the library the first important needed to know are the basics commands to read, show and write either images or videos. These commands are going to permit the user to get the image information with a certain type of data within the code, and later on modify whatever is needed on the image/video.

### Managing Images ###

As it has been commented the basic needed commands required to know at first are read, show and write. So, first have a look how to read an image.

**_Read an Image_**

  The command destined to read an image with OpenCV is called **imread(input1, input2)** and has two different inputs and one output.
 
   * The first input is to specify the path of the image that is wanted to be loaded.
   * The second input, is a flag that identifies the way the image has to be read.
	* **cv2.IMREAD_COLOR:** Loads a color image. Any transparency of image will be neglected. It is the default flag. A **_1_** can be used instead the text.
	* **cv2.IMREAD_GRAYSCALE:** Loads image in grayscale. A **_0_** can be used instead the text.
	* **cv2.IMREAD_UNCHANGED:** Loads image as such including alpha channel. A **_-1_** can be used instead whole text.

~~~python
import cv2
import numpy as num

#Load an image in color
colorimg = cv2.imread('atlantis.jpg',1)

#Load an image in grayscale
grayimg = cv2.imread('atlantis.jpg',0)
~~~

**_Shown an Image_**

It is a good way to display the image previously loaded and check all the modifications applied to it. OpenCv provides a command named **imshow(input1, input2)** which takes two arguments.

  * First input is used to give a name to the window as a string array.
  * Second input takes the image that is desired to be displayed.

To show the image a certain amount of time the **cv2.waitKey(argument1)** can be used. The argument should be specified in milliseconds and if the user states **0**, the function itself waits till a key stroke.

In the case that after the key stroke the user wants to destroy all the windows generated, the **cv2.destroyWindow()** or **cv2.destroyAllWindows()** can be used. The first command needs one argument to specify the window name as a string to delete it. The second command will just destroy all the windows generated over the code.

~~~python
# Show the colored image aforeloaded
cv2.imshow('Color', colorimg)
# Show the grayscale image aforeloaded
cv2.imshow('Gray', grayimg)

# Wait for a key stroke
cv2.waitKey(0)
# After the key stroke destroy all windows
cv2.destroyAllWindows()
~~~

**_Write a new Image_**

Another basic command needed to work with OpenCV is how to write a modified image or a new one. OpenCv has a specific command for it and it is identified by imwrite. Similarly, as the other commands it takes two arguments:

  * The first argument allows the user to set a name for the image as a string.
  * The second argument is designated to state the image wanted to be written.

~~~python
# To write a modified image
cv2.imwrite('atlantisgray.jpg', grayimg)
~~~

>> Download a [full example]({filename}/opencv-feeg6003/examples/example1.py) of the Managing Images section.
>> 

### Managing Videos ###

In a similar way as for the images OpenCv provides functions to manage the basics also on videos.

**_Read a Video_**

In order to read a video the function provided for it is called **videoCapture(input1)**. This specific function can take two different type of arguments (the function only has one input argument). 

* In the first case if the video is desired to be captured from a WebCam the argument takes integer values **0,** **-1**, **1** and so on depending the selection of the camera.

* For the second case, the argument takes a string value specifying the path of the desired video.

In this coursework how to use a pre-stored video will be covered.

~~~python
import cv2
import numpy as nump

# Read a video from a file using VideoCapture
vid = cv2.VideoCapture('video.avi')
~~~

**_Playing a video from pre-saved file_**

Playing a video in that case is not that simple as was viewing an image. The steps to follow are explained over the section:

1. Firstly, it is important to check if the video has been opened using **.isOpened()**. This function returns a Boolean. True means that the capture has been done correctly, unlike the False.

2. Secondly, once the video has been captured properly the function **read()** (to the captured video) has to be used to check if the frame is read correctly. This function returns two arguments:

  * The first one is a Boolean confirming if the frame has been read correctly.
  * The second one contains the frame information, which is a matrix containing all the information of the image at that frame.

3. Finally, as the information of the frame is provided by the read function, the **imshow()** can be used again to display the frame.

The point is that in order to show all the frame sequence, the user will need to put the read and the imshow statements inside a while to detect the end of the video. In the following example everything can be clarified.

~~~python
import cv2
import numpy as nump

# Read a video from a file using VideoCapture
vid = cv2.VideoCapture('video.avi')

# 1. Check if the video has been opened properly
open = vid.isOpened()

# 2. Read the video frames inside a loop till the end of the video
# The end of the video is stated when 'open' is false, otherwise it will be true
while(open):
	ret, frame = vid.read()

# 3. Display the video frames using imshow as for the images checking if the frame is correct, the video has to be displayed at a certain fram per second, in this case use the default value of 25 miliseconds inside the cv2.waitKey()
	if(ret):
	    cv2.imshow('video',frame)
	    cv2.waitKey(25)
	else:
	    break

# 4. Release the video and close all the windows
vid.release()
cv2.destroyAllWindows()
~~~

**_Write a video_**

As happened for playing videos, the situation is trickier than in the case of writing an image. The steps to follow are going to be explained in this section:

Initially for reading the image the steps needed are in the first section of this part (Read a video).

Secondly, the user needs to create a VideoWriter object where five arguments have to be stated.

   + First, the name of the output file.
   + Second, it is needed to define the **fourcc** using **cv2.VideoWriter_fourcc(*'code')**. The fourcc is a code of 4-bytes to specify the video codec. In this case it is recommended to use the **XVID** but the following website can be checked out to know more about. [Fourcc website](http://www.fourcc.org/codecs.php)
   + Third, the number of frame per second.
   + Fourth, the frame size.
   + Fifth and final the color flag has to be state. If is true, encoder expects color frame and if false, it will be grayscale.

 Finally, the sequence of frames has to be written using the **VideoWriter()** and the function **write()** provided by the class. This function takes one argument, and as it is logical the user needs to provide the frame to write to it.

~~~python
import cv2
import numpy as nump

vid = cv2.VideoCapture('video.avi')

#Define the fourcc code. For ubuntu it is recommended to use XVID.
fourcc = cv2.VideoWriter_fourcc(*'XVID')
# Create the VideoWriter object
vwo = cv2.VideoWriter('newVideo.avi', fourcc, 20.0, (640,480))

# Now inside the previous explained loop to show the video, it is necessary to write everysingle frame of the video
open = vid.isOpened()
while(open):
	ret, frame = vid.read()
	if(ret):
# Using the VideoWriter object thw write function is called and passed the frame argument.
# The idea is to make a modification on the video and after that process write it down in a new file.
	    vwo.write(frame)
	    cv2.waitKey(25)
	else:
	    break
...
# End the code releasing the video and destroying all the windows
~~~

>> Download a [full example]({filename}/opencv-feeg6003/examples/example2.py) of the Managing Videos section.
>> 

### Basic image and video manipulations ###

Once the basics commands to read, display and write images and videos, it is important to understand the output given by the read function.

The resultant output is a matrix that contains all the information of the whole image or the frame information when reading a video. This matrix is made off all the pixels and its information. Every pixel contains the information of the blue, green and red color. Hence, in this section the commands to access into a pixel, select a specific channel and different ways to modify the values of the pixels are going to be covered.

**_Image number of pixels_**

As it has been said, the important part of reading a video or an image is to know how to access to every pixel and do the pertinent modifications, but first it is needed to know the size of that matrix containing the information. OpenCv provides this data using **shape** where your image or frame information is stored.

The **shape()** function has three different outputs:

 + First output informs the number of rows.
 + Second output informs about the number of columns.
 + Third one identifies the number of channels of the image. If the user is dealing with an image color the number of channels should be 3, otherwise if the image is grayscale any kind of information will be provided as a third output.

This function can be used in a similar way to get the video shape information.

~~~python
# From the previous image colorimg
print colorimg.shape
540,810,3
~~~

**_Getting the pixel information_**

At this point you have to be able to know the size of your image or video frame. Consequently, you are able to go through all of the pixels and gather the needed information.

To access a pixel information the only necessary thing is to specify a row and a column, which identifies a single pixel all over the matrix. The pixel contains three values and as OpenCV works with the BGR display these values are understood as:

 * The first pixel gives the blue value.
 * The second pixel gives the green value.
 * The third pixels gives the red value.

~~~python
# From the previous image 'colorimg'
pixel = colorimg[50,80]
print pixel
[157 135 137]
~~~

**_Selecting only one channel_**

Instead of working with the three channels OpenCV provides an option to work with all the channels separately with the function **cv2.split(input1)**.

The function split takes on input argument which is the image to split the cannels and three different outputs. In this case the three outputs are the matrix of blue, green and red values.

If the user wants to get back to the genuine image the **cv2.merge(input1)**, where the **input1** are the three different channels.

~~~python
# The split function of an image returns the three different channels separated
b,g,r = cv2.split(colorimg)
# To get again the genuine matrix the merge funtion can be used
colorimg_merged = cv2.merge((b,g,r))
~~~

**_Different ways of modifying pixels_**

Up to this point, everyone should be able to read, know the size of the read image and access to the pixels on it. So, now it is time to show how to modify the pixel value. To do so there are different ways to do it.

The first way to do it is just selecting a pixel and giving a specific value as a vector of three numerical components. Normally this way of modification is oriented to transform a set of pixels not only one.

~~~python
colorimg[100,100] = [255,255,255]
print colorimg[100,100]
[255 255 255]
~~~

The second way to do it is using the **numpy** library and its function **itemset()** for arrays. The problem is to modify the three different channels, three different calls to that function are needed but it is considered to be faster for single pixels than the previous way.
~~~python
# modifying RED value
colorimg.itemset((10,10,2),100)
colorimg.item(10,10,2)
100
~~~

Get the full code of the Basic image and video manipulations:[basicmanipulations]({filename}/opencv-feeg6003/examples/example3.py).

### Changing Colors Spaces ###

**_Overview_**

<p>The image in OpenCV is stored as a matrix that contains the position of each pixel and the color channels. The color channels is also called color spaces and there are three common forms which are Gray, BGR and HSV.</p>
<p>Gray is one that only has one channel which is the brightness of pixel. It can only display a black-and-white picture while BGR and HSV contains three channels and can show a colored picture.</p>
<p>BGR is basically a process that mixes three basic colors of blue, green, red to get a different color by controlling how much each color is used. The range for each channel is [0,255]. On the other hand, HSV consists of three channels that are Hue, Saturation and Value. Hue represents the colors, Saturation represents the amount of gray and Value is the brightness value. The range of Hue is [0,179] and the ranges of the others are [0,255].</p>

**_Changing Colorspaces_**

<p>In OpenCV, a function <b>cv.cvtColor(input_image,flag)</b> can convert the color-space between different forms.</p>
<p>For BGR to Gray, the flag is <b>cv.COLOR_BGR2GRAY</b>. For BGR to HSV, the flag is <b>cv.COLOR_BGR2HSV</b>.</p>
>>[Example for Converting Colorspaces section.]({filename}/opencv-feeg6003/examples/cvtcolo.py)
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_gray.png" alt="gray" width="300" height="300"><br>
Gray
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_bgr.png" alt="bgr" width="300" height="300"><br>
BGR
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_hsv.png" alt="hsv" width="300" height="300"><br>
HSV
</div>
<p>We can see that <b>cv.imshow()</b> can only display a picture in BGR mode or Gray mode.</p>
<p>You can also get other flags if you want. To check it, simply use the code below:</p>

~~~python
import cv2 as cv
flags = [i for i in dir(cv) if i.startswith('COLOR_')]
print(flags)
~~~

<br>

**_Extracting Certain Objects by Color_**

<p>To achieve this, we always use the HSV color mode. In HSV mode, it is easier to know the range of a certain color because we only need to modify the range of Hue.</p>
<p>Get the HSV values of blue:</p>

~~~python
blue = np.uint8([[[255,0,0]]])
hsv_blue = cv.cvtColor(blue,cv.COLOR_BRG2HSV)
print(hsv_blue)
~~~

<p>After doing this, we can extract a color from a picture by several steps:</p>
* Convert the picture from BGR to HSV<br>
* Threshold the HSV picture for a range of the color<br>
* Extract objects<br>

<p>Here, we use two functions <b>cv.inRange()</b> and <b>cv.bitwise_and()</b>.</p>
* <b>cv.inRange(image_input,lower_boundary,upper_boundary)</b> can check if every element in image_input lies in the range. It will set the element to be 255 if it is and set it to be 0 if not. As we only modify the value of Hue to get the range, the output of cv.inRange() should be a black-white picture.
* <b>cv.bitwise_and()</b> perform an algorithm that get the product of each element of two images bit-wisely.

>>[Example for Extracting Certain Objects by Color]({filename}/opencv-feeg6003/examples/extcolor.py)

<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_bgr.png" alt="bgr" width="400" height="300"><br>
Source
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_mask.png" alt="mask" width="400" height="300"><br>
Mask
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_result.png" alt="result" width="400" height="300"><br>
Result
</div>

### Geometric Transformations ###

**_Overview_**

<p>Transformation basically consists of three operations which are scaling, translation and rotation. As images in OpenCV are stored as matrices that contain the positions of pixels and the channel information. We can simply use the matrix computation to transform images.</p>
<p>Here is a rough idea of how to perform transformation on images.</p>
<p>Generally, the position of a pixel in an image is:</p>

<div style="text-align:center">
<img src="/opencv-feeg6003/images/eq1.PNG">
</div>

<p>The transformations used for different situations can be described as:</p>

<div style="text-align:center">
<img src="/opencv-feeg6003/images/eq2.PNG">
</div>

<div style="text-align:center">
<img src="/opencv-feeg6003/images/eq3.PNG">
</div>

<p>Performing an algorithm below, we can get the new position of the pixel.</p>

<div style="text-align:center">
<img src="/opencv-feeg6003/images/eq5.PNG">
</div>

<p>By performing the same process on each pixel, we can transform the whole image.</p>

**_Scaling_**

<p>Scaling is the operation that resizes the image. In OpenCV, we use the function <b>cv.resize(img,None,sx,sy,interpolation)</b> to complete it.</p>
* <b>img</b>: source image
* <b>sx, sy</b>: scale along X-axis and Y-axis
* <b>interpolation</b>: interpolation method (will be explained after)
<p>The transformation matrix here is:</p>

<div style="text-align:center">
<img src="/opencv-feeg6003/images/eq6.PNG">
</div>

<p>Using the algorithm, we can easily get new positions of pixels either in a bigger image or a smaller one. However, the number of pixelx is fixed, so we need interpolation to either fill the vacancies or condense pixels. Different methods are used here.</p>
* <b>cv.INTER_AREA</b>: for shrinking<br>
* <b>cv.INTER_CUBIC</b>: for zooming but is slow<br>
* <b>cv.INTER_LINEAR</b>: for zooming and is the default method<br>
>>[Scaling an image example]({filename}/opencv-feeg6003/examples/scale.py)

<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_bgr.png" alt="bgr" width="400" height="300"><br>
Source
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/scale.png" alt="scale" width="600" height="450"><br>
Scaled
</div>

**_Translation_**

<p>Translation is actually moving every pixel in the image. In OpenCV, we use displacements along X-axis and Y-axis to complete this process.</p>
<p>The transformation matrix here is:</p>

<div style="text-align:center">
<img src="/opencv-feeg6003/images/eq7.PNG">
</div>

<p>The function we used here is <b>cv.warpAffine(img,M,area)</b> which contains three arguments.</p>
* <b>img</b>: source image<br>
* <b>M</b>: the transformation matrix<br>
* <b>area</b>: the display area of new image with the form of (width, height)<br>
<p>Having this function, we can pass the matrix to it once we know the displacements.</p>

>>[Example for Translation]({filename}/opencv-feeg6003/examples/translate.py)

<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_bgr.png" alt="bgr" width="400" height="300"><br>
Source
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/translate.png" alt="translate" width="400" height="300"><br>
Translated
</div>

**_Rotation_**

<p>Rotation of an image can also be achieved by matrix algorithm.</p>
<p>When rotating with the original center, the transformation matrix is:</p>

<div style="text-align:center">
<img src="/opencv-feeg6003/images/eq8.PNG">
</div>

<p>When rotating with adjustable center, the process can be seperated into three steps:</p>
* translate from original point to rotation center<br>
* rotate with center<br>
* translate backward<br>
<p>In OpenCV, we can use <b>cv.getRotationMatrix2D()</b> to get the rotation matrix with adjustable center. The three arguments in function <b>cv.getRotationMatrix2D()</b> are rotation center(given in brackets), angle and scale which means that the matrix also contains the function of scaling. Once we get the rotation matrix, we can pass it to function <b>cv.warpAffine()</b> to complete rotation.</p>

>>[Example for Rotation]({filename}/opencv-feeg6003/examples/rotate.py)

<div style="text-align:center">
<img src="/opencv-feeg6003/examples/change_bgr.png" alt="bgr" width="400" height="300"><br>
Source
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/rotate.png" alt="rotate" width="400" height="300"><br>
Rotated
</div>

### CONTOURS AND HISTOGRAMS ###

#### Contours ####

<p>In OpenCV, contours can be seen as curves that connect all the continuous points along the boundary with the same color and intensity. Here, I will simply introduce contours.</p>

**_Finding contours_**

<p>To achieve finding contours, we use the function <b>cv.findContours(img,mode,method)</b></p>
* <b>img</b>: source image<br>
* <b>mode</b>: contour retrieval mode<br>
* <b>method</b>: contour approximation method<br>
<p>This function is like finding a white object in a black background.
It will perform better if use the funciton <b>cv.threshold()</b> to convert image to a binary image.</p>
<p>The output contours is actually a list of points. 
The hierarchy is affected by the contour retrieval mode and represents the ranks of contours. 
Generally speaking, it represents the parent-child relationship where external contours are parents. For more details of hierarchy, check the link <a href="https://docs.opencv.org/3.4.0/d9/d8b/tutorial_py_contours_hierarchy.html">here</a>.</p>
<p>For the approximation methods, there are two commonly used methods:</p>
* <b>cv.CHAIN_APPROX_NONE</b>: returns all points<br>
* <b>cv.CHAIN_APPROX_SIMPLE</b>: returns less points<br>
<p>If the shape of contour is not very complex, using <b>cv.CHIAN_APPROX_SIMPLE</b> can save time effectively.</p>

**_Drawing contours_**

<p>Use <b>cv.drawContours(img,contours,index,(color),thickness)</b>:</p>
* <b>img</b>: source image<br>
* <b>contours</b>: the list of contour points<br>
* <b>index</b>: index of contour(use -1 to draw all contours or other number to draw certain contour)<br>
* <b>color</b>: color in BGR mode<br>
* <b>thickness</b>: thickness of contours<br>
>>[Contours example]({filename}/opencv-feeg6003/examples/contour.py)

<div style="text-align:center">
<img src="/opencv-feeg6003/examples/contours_1.png" alt="allcontours" width="400" height="300"><br>
All contours
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/contours_2.png" alt="certaincontour" width="400" height="300"><br>
Certain contour
</div>

#### Histogram ####

Histogram is a plot which shows the intensity distribution of an image. The pixel value is on X-axis and number of pixels is on Y-axis.

Here is an example of histogram from <a href="http://docs.opencv.org">official website</a>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/histogram_sample.jpg">
</div>

In OpenCV, we use the function cv.calcHist(image,[channels],mask,histSize,ranges)

* **image**: source image
* **channels**: index of channel for calculating histogram(given in square brackets)
* **mask**: mask image used to restrict the area to calculate(None for full area)
* **histSize**: the size of bin(seperate the whole value interval into subintervals with same size)
* **ranges**: the value range to calculate(normally [0,256])

>>[Example: Calculate histogram in OpenCV]({filename}/opencv-feeg6003/examples/histogram.py)

<div style="text-align:center">
<img src="/opencv-feeg6003/examples/southampton.jpg" alt="southampton" width="600" height="450"><br>
Source
</div>
<div style="text-align:center">
<img src="/opencv-feeg6003/examples/histogram.png" alt="histogram" width="600" height="450"><br>
Histogram
</div>

### EXTRA EXAMPLES - Feature detection, description and matching ###

There are a bunch of algorithms incorporated in OpenCV to perform feature detection. As this workshop has a finite time we wanted to introduce you all to a couple of them. In case you are interested in you can have a look to the main page to get a deeper sense for all of them.

We have though that will be interesting to have a look to a corner detection and tracking objects.

**_Basics on corner detection_**

The basic idea of corner detection falls in moving a patch all over the image. Hence, detecting for example a bit of the interior of a green square is difficult because all of it is green and uniform. Secondly, imagine one of the sides is desired to be detected. This is going to be easier to detect but difficult to locate the exact position. Finally, find out a single corner is easy because wherever the patch is analyzing the image it will look different.

![feature]({filename}/opencv-feeg6003/images/feature.png )

* **Harris corner detection** is one of the simplest algorithms to do feature detection. There are many more refined algorithms to improve the results. Check the following link for further details of the provided algorithms by OpenCV. [Feature detection algorithms.](https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_feature2d/py_table_of_contents_feature2d/py_table_of_contents_feature2d.html)

### Video analysis ###

**_Tracking with meanshift and camshift_**

Before this chapter we will have already presented the color tracking (changing colorspaces tab), but now the algorithm will be able to track the feature through all the video frames.

The simple idea falls into defining a target to be followed, this image is transformed into a backprojected histogram (like filtering the target and deleting the rest) where the high density of points turns out into our target. Due to the movement of the target the high density of points is also moved. At this point, the algorithm looks for again the high-density region moving the pre-defined window. This process is repeated over and over till the window finds the maximum density region of points.

The idea of both algorithms is the same the only difference is that **Camshift** calculates the orientation of the target and resizes the window identifying it. **Camshift** is considerated an upgraded version of the **Meanshift**.

* For further information and examples visit [OpenCV guide for meanshift and camshift](https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_video/py_meanshift/py_meanshift.html#meanshift)
 








