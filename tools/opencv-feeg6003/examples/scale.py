import numpy as np
import cv2 as cv
img = cv.imread('logo.png')
res = cv.resize(img,None,fx=1.5,fy=1.5,interpolation = cv.INTER_CUBIC)
cv.imshow('Scaling',res)
cv.waitKey(0)
cv.destroyAllWindows()
#Or
#height, width = img.shape[:2]
#res = cv.resize(img,(1.5*width,1.5*height),interpolation = cv.INTER_CUBIC)
