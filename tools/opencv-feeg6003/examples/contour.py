import numpy as np
import cv2 as cv
img = cv.imread('logo.png')
imgray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
ret, thresh = cv.threshold(imgray,170,255,0)
im2, contours, hierarchy = cv.findContours(thresh,cv.RETR_TREE,cv.CHAIN_APPROX_SIMPLE)
#draw all contours
cv.drawContours(img,contours,-1,(255,0,0),3)
#draw certain contour
cv.drawContours(img,contours,1,(0,255,0),3)
#or use another form
cnt = contours[1]
cv.drawContours(img,[cnt],0,(0,255,0),3)#red color
cv.imshow('img',img)
cv.waitKey(0)
cv.destroyAllWindows()
